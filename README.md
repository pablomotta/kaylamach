<ul class="list-style col-xs-offset-1 col-xs-10 col-sm-offset-1 col-sm-5 qualifying-questions-1">
    <li>Are you having feelings that are too intense about life?</li>
    <li>Have you suffered a trauma that you can’t seem to stop thinking about?</li>
    <li>Do you have unexplained and recurrent headaches, stomach-aches or a rundown immune system?</li>
</ul>
<ul class="list-style col-xs-offset-1 col-xs-10 col-sm-5 qualifying-questions-2">
    <li>Are you’re using a substance to cope with life?</li>
    <li>Do you feel disconnected from previously beloved activities?</li>
    <li>Are any of your relationships strained?</li>
    <li>Have your friends and/or family told you they’re concerned about you?</li>
</ul>
