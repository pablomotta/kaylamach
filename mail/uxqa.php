<?php
// Check for empty fields
if(empty($_POST['one'])      ||
   empty($_POST['two'])     ||
   empty($_POST['three'])     ||
   empty($_POST['four'])   ||
   empty($_POST['five'])   ||
   empty($_POST['six'])   ||
   empty($_POST['seven']))
   {
   echo "No arguments Provided!";
   return false;
   }

$one = strip_tags(htmlspecialchars($_POST['one']));
$two = strip_tags(htmlspecialchars($_POST['two']));
$three = strip_tags(htmlspecialchars($_POST['three']));
$four = strip_tags(htmlspecialchars($_POST['four']));
$five = strip_tags(htmlspecialchars($_POST['five']));
$six = strip_tags(htmlspecialchars($_POST['six']));
$seven = strip_tags(htmlspecialchars($_POST['seven']));

// Create the email and send the message
$to = 'pablo@pablomotta.com'; //
// $email_address = 'noreply@pablomotta.com';
$email_subject = "UX Q&A answers for kaylamach.com";
$email_body = "You have received a UX Q&A answers for kaylamach.com.\n\n"."Here are the answers:\n\n
1. Would you recommend this website to a friend?\n\n $one\n\n\n\n
2. Were you able to find all the information you’d want in a Therapist’s website?\n\n $two\n\n\n\n
3. How would you describe the website in one or more words?\n\n $three\n\n\n\n
4. If you were to review website what score would you give it out of 10?\n\n $four\n\n\n\n
5. What do you find most frustrating about the website?\n\n $five\n\n\n\n
6. If you could change one thing about website what would it be and why?\n\n $six\n\n\n\n
7. How can we improve the website? Send us your ideas and suggestions.\n\n $seven";
$headers = "From: Website Survey Page\n";
mail($to,$email_subject,$email_body,$headers);
return true;
?>
