// Agency Theme JavaScript

(function($) {
    "use strict"; // Start of use strict

    // jQuery for page scrolling feature - requires jQuery Easing plugin
    $('a.page-scroll').bind('click', function(event) {
        var $anchor = $(this);
        $('html, body').stop().animate({
            scrollTop: ($($anchor.attr('href')).offset().top - 50)
        }, 1250, 'easeInOutExpo');
        event.preventDefault();
    });

    // Highlight the top nav as scrolling occurs
    $('body').scrollspy({target: '.navbar-fixed-top', offset: 51});

    // Closes the Responsive Menu on Menu Item Click
    $('.navbar-collapse ul li a').click(function() {
        $('.navbar-toggle:visible').click();
        $('#close').hide();
        $('#open').show();

    });

    // Offset for Main Navigation
    $('#mainNav').affix({
        offset: {
            top: 100
        }
    })

})(jQuery); // End of use strict

// Tech Scroll

window.sr = ScrollReveal();
sr.reveal('.kayla', {
    duration: 2000,
    origin: 'bottom'
});
sr.reveal('.bottom-slide', {
    duration: 2000,
    origin: 'bottom',
    distance: '100px'
});
sr.reveal('.intro-text', {
    duration: 2000,
    origin: 'top'
});
sr.reveal('.section-heading', {
    duration: 2000,
    origin: 'top',
    distance: '50px'
});
sr.reveal('.right-slide', {
    duration: 2000,
    origin: 'bottom',
    distance: '100px'
});
sr.reveal('.left-slide', {
    duration: 2000,
    origin: 'bottom',
    distance: '100px'
});

//Unused ----------------------------------

// sr.reveal('.showcase-btn', {
//   duration: 2000,
//   delay: 2000,
//   origin:'bottom'
// });
// sr.reveal('#testimonial div', {
//   duration: 2000,
//   origin:'bottom'
// });
// sr.reveal('.info-left', {
//   duration: 2000,
//   origin:'left',
//   distance:'300px',
//   viewFactor: 0.2
// });
// sr.reveal('.info-right', {
//   duration: 2000,
//   origin:'right',
//   distance:'300px',
//   viewFactor: 0.2
// });
